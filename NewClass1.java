package javaapplication3;

import java.util.Scanner;

public class NewClass1 {

    static String score(String str) {

        int count = 0;
        int max = 0;
        String word = "";
        String maxWord = "";

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == ' ') {
                count = 0;
                word = "";
                continue;
            }
            word += str.charAt(i);
            int score = str.charAt(i) - 96;
            count += score;
            if (count > max) {
                max = count;
                maxWord = word;
            }
        }

        return maxWord;

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str1 = sc.nextLine();
        System.out.println(score(str1));
    }
}
