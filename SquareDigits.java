package javaapplication3;

import java.util.Scanner;

public class SquareDigits {

    public static String SquareDigits(int num) {
        String[] digits = String.valueOf(num).split("");
        String ans = "";

        for (String digit : digits) {
            ans += (int) Math.pow(Integer.parseInt(digit), 2);
        }

        return ans;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n;
        n = sc.nextInt();
        System.out.println(SquareDigits(n));
    }

}
