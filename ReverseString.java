/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication3;

import java.util.Scanner;

public class ReverseString {

    public static String ReverseString(String str) {
        String[] words = str.split(" ");

        String revString = "";

        for (int i = 0; i < words.length; i++) {

            String word = words[i];
            String revWord = "";

            for (int j = word.length() - 1; j >= 0; j--) {
                revWord = revWord + word.charAt(j);
            }

            revString = revString + revWord + " ";
        }
        return revString;
    }

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        String str;
        str = sc.nextLine();
        System.out.println(ReverseString(str));

    }
}
